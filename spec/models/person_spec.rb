require 'rails_helper'

describe Person do
  it "has a name that is made up of first and last names" do
    person = Person.create
    person.first_name = "First"
    person.last_name = "Last"
    expect(person.name).to eq("First Last")
  end
end
