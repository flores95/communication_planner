class TasksController < ApplicationController
  before_action :check_for_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.all
    @tasks_title = "All Tasks"
  end

  def active
    @tasks = Task.active_tasks
    @tasks_title = "Active Tasks"
    render action: :index
  end

  def past
    @tasks = Task.past_tasks
    @tasks_title = "Past Tasks"
    render action: :index
  end

  def user
    selected_user = User.find(params[:user])
    @tasks = Task.user_tasks(selected_user)
    @tasks_title = (selected_user.id == session[:user_id]) ? "My Tasks" : selected_user.name + " Tasks"
    render action: :index
  end

  def destination 
    selected_destination = Destination.find(params[:destination])
    @tasks = Task.destination_tasks(selected_destination)
    @tasks_title = selected_destination.name + " Tasks"
    render action: :index
  end

  def audience
    selected_audience = Audience.find(params[:audience])
    @tasks = Task.audience_tasks(selected_audience)
    @tasks_title = selected_audience.name + " Tasks"
    render action: :index
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to @task, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:name, :start_date, :end_date, :event_id, :audience_id, :destination_id, :owner_id)
    end
end
