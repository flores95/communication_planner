class PeopleListsController < ApplicationController
  before_action :check_for_user!
  before_action :set_people_list, only: [:show, :edit, :update, :destroy]

  # GET /people_lists
  # GET /people_lists.json
  def index
    @people_lists = PeopleList.all
  end

  # GET /people_lists/1
  # GET /people_lists/1.json
  def show
  end

  # GET /people_lists/new
  def new
    @people_list = PeopleList.new
  end

  # GET /people_lists/1/edit
  def edit
  end

  # POST /people_lists
  # POST /people_lists.json
  def create
    @people_list = PeopleList.new(people_list_params)

    respond_to do |format|
      if @people_list.save
        format.html { redirect_to @people_list, notice: 'People list was successfully created.' }
        format.json { render :show, status: :created, location: @people_list }
      else
        format.html { render :new }
        format.json { render json: @people_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people_lists/1
  # PATCH/PUT /people_lists/1.json
  def update
    respond_to do |format|
      if @people_list.update(people_list_params)
        format.html { redirect_to @people_list, notice: 'People list was successfully updated.' }
        format.json { render :show, status: :ok, location: @people_list }
      else
        format.html { render :edit }
        format.json { render json: @people_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people_lists/1
  # DELETE /people_lists/1.json
  def destroy
    @people_list.destroy
    respond_to do |format|
      format.html { redirect_to people_lists_url, notice: 'People list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_people_list
      @people_list = PeopleList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def people_list_params
      params.require(:people_list).permit(:name, :public)
    end
end
