class DashboardController < ApplicationController
  before_action :check_for_user!

  def index
      @events = Event.all
      @tasks = Task.all
  end
end
