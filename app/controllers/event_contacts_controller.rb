class EventContactsController < ApplicationController
  before_action :check_for_user!
  before_action :set_event_contact, only: [:show, :edit, :update, :destroy]

  # GET /event_contacts
  # GET /event_contacts.json
  def index
    @event_contacts = EventContact.all
  end

  # GET /event_contacts/1
  # GET /event_contacts/1.json
  def show
  end

  # GET /event_contacts/new
  def new
    @event_contact = EventContact.new
  end

  # GET /event_contacts/1/edit
  def edit
  end

  # POST /event_contacts
  # POST /event_contacts.json
  def create
    @event_contact = EventContact.new(event_contact_params)

    respond_to do |format|
      if @event_contact.save
        format.html { redirect_to event_path(@event_contact.event), notice: 'Event contact was successfully created.' }
        format.json { render :show, status: :created, location: @event_contact }
      else
        format.html { render :new }
        format.json { render json: @event_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_contacts/1
  # PATCH/PUT /event_contacts/1.json
  def update
    respond_to do |format|
      if @event_contact.update(event_contact_params)
        format.html { redirect_to @event_contact, notice: 'Event contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @event_contact }
      else
        format.html { render :edit }
        format.json { render json: @event_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_contacts/1
  # DELETE /event_contacts/1.json
  def destroy
    @event_contact.destroy
    respond_to do |format|
      format.html { redirect_to event_contacts_url, notice: 'Event contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_contact
      @event_contact = EventContact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_contact_params
      params.require(:event_contact).permit(:role, :event_id, :person_id)
    end
end
