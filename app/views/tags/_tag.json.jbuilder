json.extract! tag, :id, :name, :public, :created_at, :updated_at
json.url tag_url(tag, format: :json)