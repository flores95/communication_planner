json.extract! destination_type, :id, :name, :created_at, :updated_at
json.url destination_type_url(destination_type, format: :json)