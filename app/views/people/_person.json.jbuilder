json.extract! person, :id, :name, :first_name, :last_name, :email, :mobile_phone, :created_at, :updated_at
json.url person_url(person, format: :json)