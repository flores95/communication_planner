json.extract! task, :id, :name, :start_date, :end_date, :event_id, :audience_id, :destination_id, :owner_id, :created_at, :updated_at
json.url task_url(task, format: :json)