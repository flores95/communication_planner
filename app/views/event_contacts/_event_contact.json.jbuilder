json.extract! event_contact, :id, :role, :event_id, :person_id, :created_at, :updated_at
json.url event_contact_url(event_contact, format: :json)