json.extract! audience, :id, :name, :description, :created_at, :updated_at
json.url audience_url(audience, format: :json)