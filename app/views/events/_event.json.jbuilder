json.extract! event, :id, :name, :description, :start_date, :start_time, :end_date, :end_time, :all_day, :public, :created_at, :updated_at
json.url event_url(event, format: :json)