json.extract! people_list, :id, :name, :public, :created_at, :updated_at
json.url people_list_url(people_list, format: :json)