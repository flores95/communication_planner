json.extract! destination, :id, :name, :destination_type_id, :description, :created_at, :updated_at
json.url destination_url(destination, format: :json)