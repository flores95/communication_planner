class DestinationType < ApplicationRecord

  def destinations
    return Destination.where "destination_type_id = ?", self.id
  end
end
