class User < ApplicationRecord
  belongs_to :person

  def name
    return self.person.blank? ? " " : self.person.name; 
  end

  def authenticate pwd
    pwd == self.password ? true : false 
  end

end
