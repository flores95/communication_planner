class EntityTag < ApplicationRecord
  belongs_to :basic_entity
  belongs_to :tag
end
