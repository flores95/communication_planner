class Event < ApplicationRecord
  has_many :tasks

  def start_date_pretty
    return self.start_date.strftime("%m/%d/%Y")
  end

  def start_time_pretty
    return self.start_time.strftime("%I:%M:%S %p")
  end

  def start_date_and_time
    self.start_date && self.start_time ? self.start_date.strftime("%m/%d/%Y") + " " + self.start_time.strftime("%I:%M:%S %p") : ""
  end

  def end_date_pretty
    return self.end_date.strftime("%m/%d/%Y")
  end

  def end_time_pretty
    return self.end_time.strftime("%I:%M:%S %p")
  end

  def end_date_and_time
    return self.end_date.strftime("%m/%d/%Y") + " " + self.end_time.strftime("%I:%M:%S %p")
  end

  def days_to_start
    return self.start_date > Date.today ? (self.start_date - Date.today).to_i : 0;
  end

  def days_to_end
    return self.end_date > Date.today ? (self.end_date - Date.today).to_i : 0;
  end

  def self.active_events
    return Event.where "end_date >= ?", Date.today.to_date
  end

  def self.past_events
    return Event.where "end_date < ?", Date.today.to_date
  end

  def self.planned_events
    event_array = []
    Event.active_events.each do |active_event|
      if !active_event.tasks.blank? 
        event_array << active_event
      end
    end
    return event_array
  end

  def self.count_planned
    return Event.planned_events.count
  end

  def self.unplanned_events
    event_array = []
    Event.active_events.each do |active_event|
      if active_event.tasks.blank? 
        event_array << active_event
      end
    end
    return event_array
  end

  def self.count_unplanned
    return Event.unplanned_events.count
  end

end
