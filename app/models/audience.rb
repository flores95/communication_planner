class Audience < ApplicationRecord

  def active_tasks
    return Task.audience_tasks(self)
  end
end
