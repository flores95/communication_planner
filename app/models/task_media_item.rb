class TaskMediaItem < ApplicationRecord
  belongs_to :task
  belongs_to :media_item
end
