class Destination < ApplicationRecord
  belongs_to :destination_type

  def active_tasks
    return Task.destination_tasks(self);
  end

end
