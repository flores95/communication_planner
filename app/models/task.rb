class Task < ApplicationRecord
  belongs_to :event
  belongs_to :audience
  belongs_to :destination
  belongs_to :owner, class_name: "User"

  def days_to_start
    return (self.start_date && self.start_date > Date.today) ? (self.start_date - Date.today).to_i : 0;
  end

  def days_to_end
    return self.end_date > Date.today ? (self.end_date - Date.today).to_i : 0;
  end

  def self.active_tasks 
    Task.where "end_date >= ?", Date.today.to_date
  end

  def self.past_tasks 
    Task.where "end_date < ?", Date.today.to_date
  end

  def self.user_tasks user
    Task.where "owner_id = ? AND end_date >= ?", user.id, Date.today.to_date
  end

  def self.destination_tasks destination
    Task.where "destination_id = ? AND end_date >= ?", destination.id, Date.today.to_date
  end

  def self.audience_tasks audience
    Task.where "audience_id = ? AND end_date >= ?", audience.id, Date.today.to_date
  end

end
