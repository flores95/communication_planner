class Person < BasicEntity

  def name
    "#{self.first_name} #{self.last_name}"
  end
  
  def user
    User.find_by(person_id: self.id)
  end
  
  def active_tasks
    if self.user
      return Task.user_tasks(self.user) 
    else
      return []
    end
  end

end
