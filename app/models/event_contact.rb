class EventContact < ApplicationRecord
  belongs_to :event
  belongs_to :person
  validates_uniqueness_of :role, scope: [:event_id, :person_id]

  def name
    return self.event.name + "::" + self.person.name
  end

  def self.contacts_for_event event
    return EventContact.where event: event
  end

end
