Rails.application.routes.draw do
  get      '/login',   to: 'sessions#new'
  post     '/login',   to: 'sessions#create'
  delete   '/logout',  to: 'sessions#destroy'

  root 'dashboard#index'
  resources :audiences
  resources :tasks do
    collection do
      get :active
      get :past
      get :user
      get :destination
      get :audience
    end
  end
  resources :event_contacts
  resources :media_items
  resources :destinations do
    collection do
      get :destination_type
    end
  end
  resources :destination_types
  resources :users
  resources :events do
    collection do
      get :active
      get :past
      get :planned
      get :unplanned
    end
  end
  resources :people_lists
  resources :tags
  resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
