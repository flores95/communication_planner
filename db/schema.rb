# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170811194244) do

  create_table "audiences", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "destination_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "destinations", force: :cascade do |t|
    t.string   "name"
    t.integer  "destination_type_id"
    t.text     "description"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["destination_type_id"], name: "index_destinations_on_destination_type_id"
  end

  create_table "entity_tags", force: :cascade do |t|
    t.integer  "basic_entity_id"
    t.string   "basic_entity_type"
    t.integer  "tag_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["basic_entity_id"], name: "index_entity_tags_on_basic_entity_id"
    t.index ["tag_id"], name: "index_entity_tags_on_tag_id"
  end

  create_table "event_contacts", force: :cascade do |t|
    t.string   "role"
    t.integer  "event_id"
    t.integer  "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_contacts_on_event_id"
    t.index ["person_id"], name: "index_event_contacts_on_person_id"
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.date     "start_date"
    t.time     "start_time"
    t.date     "end_date"
    t.time     "end_time"
    t.boolean  "all_day"
    t.boolean  "public"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "media_items", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.binary   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "mobile_phone"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "people_lists", force: :cascade do |t|
    t.string   "name"
    t.boolean  "public"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.boolean  "public"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "task_media_items", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "media_item_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["media_item_id"], name: "index_task_media_items_on_media_item_id"
    t.index ["task_id"], name: "index_task_media_items_on_task_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "name"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "event_id"
    t.integer  "audience_id"
    t.integer  "destination_id"
    t.integer  "owner_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["audience_id"], name: "index_tasks_on_audience_id"
    t.index ["destination_id"], name: "index_tasks_on_destination_id"
    t.index ["event_id"], name: "index_tasks_on_event_id"
    t.index ["owner_id"], name: "index_tasks_on_owner_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "user_name"
    t.string   "password"
    t.integer  "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_users_on_person_id"
  end

end
