class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.date :start_date
      t.time :start_time
      t.date :end_date
      t.time :end_time
      t.boolean :all_day
      t.boolean :public

      t.timestamps
    end
  end
end
