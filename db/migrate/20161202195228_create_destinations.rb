class CreateDestinations < ActiveRecord::Migration[5.0]
  def change
    create_table :destinations do |t|
      t.string :name
      t.references :destination_type, foreign_key: true
      t.text :description

      t.timestamps
    end
  end
end
