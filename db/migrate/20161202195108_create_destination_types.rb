class CreateDestinationTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :destination_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
