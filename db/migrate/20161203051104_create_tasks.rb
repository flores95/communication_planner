class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.references :event, foreign_key: true
      t.references :audience, foreign_key: true
      t.references :destination, foreign_key: true
      t.references :owner, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
