class CreateTaskMediaItems < ActiveRecord::Migration[5.0]
  def change
    create_table :task_media_items do |t|
      t.references :task, foreign_key: true
      t.references :media_item, foreign_key: true

      t.timestamps
    end
  end
end
