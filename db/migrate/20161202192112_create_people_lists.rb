class CreatePeopleLists < ActiveRecord::Migration[5.0]
  def change
    create_table :people_lists do |t|
      t.string :name
      t.boolean :public

      t.timestamps
    end
  end
end
