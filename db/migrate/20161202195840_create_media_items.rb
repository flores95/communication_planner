class CreateMediaItems < ActiveRecord::Migration[5.0]
  def change
    create_table :media_items do |t|
      t.string :name
      t.string :url
      t.binary :file

      t.timestamps
    end
  end
end
