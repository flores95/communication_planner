class CreateEntityTags < ActiveRecord::Migration[5.0]
  def change
    create_table :entity_tags do |t|
      t.references :basic_entity, foreign_key: true
      t.string :basic_entity_type
      t.references :tag, foreign_key: true

      t.timestamps
    end
  end
end
