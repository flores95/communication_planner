require 'test_helper'

class PeopleListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @people_list = people_lists(:one)
  end

  test "should get index" do
    get people_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_people_list_url
    assert_response :success
  end

  test "should create people_list" do
    assert_difference('PeopleList.count') do
      post people_lists_url, params: { people_list: { name: @people_list.name, public: @people_list.public } }
    end

    assert_redirected_to people_list_url(PeopleList.last)
  end

  test "should show people_list" do
    get people_list_url(@people_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_people_list_url(@people_list)
    assert_response :success
  end

  test "should update people_list" do
    patch people_list_url(@people_list), params: { people_list: { name: @people_list.name, public: @people_list.public } }
    assert_redirected_to people_list_url(@people_list)
  end

  test "should destroy people_list" do
    assert_difference('PeopleList.count', -1) do
      delete people_list_url(@people_list)
    end

    assert_redirected_to people_lists_url
  end
end
