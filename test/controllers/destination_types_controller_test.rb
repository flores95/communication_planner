require 'test_helper'

class DestinationTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @destination_type = destination_types(:one)
  end

  test "should get index" do
    get destination_types_url
    assert_response :success
  end

  test "should get new" do
    get new_destination_type_url
    assert_response :success
  end

  test "should create destination_type" do
    assert_difference('DestinationType.count') do
      post destination_types_url, params: { destination_type: { name: @destination_type.name } }
    end

    assert_redirected_to destination_type_url(DestinationType.last)
  end

  test "should show destination_type" do
    get destination_type_url(@destination_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_destination_type_url(@destination_type)
    assert_response :success
  end

  test "should update destination_type" do
    patch destination_type_url(@destination_type), params: { destination_type: { name: @destination_type.name } }
    assert_redirected_to destination_type_url(@destination_type)
  end

  test "should destroy destination_type" do
    assert_difference('DestinationType.count', -1) do
      delete destination_type_url(@destination_type)
    end

    assert_redirected_to destination_types_url
  end
end
