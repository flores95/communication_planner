require 'test_helper'

class EventContactsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event_contact = event_contacts(:one)
  end

  test "should get index" do
    get event_contacts_url
    assert_response :success
  end

  test "should get new" do
    get new_event_contact_url
    assert_response :success
  end

  test "should create event_contact" do
    assert_difference('EventContact.count') do
      post event_contacts_url, params: { event_contact: { event_id: @event_contact.event_id, person_id: @event_contact.person_id, role: @event_contact.role } }
    end

    assert_redirected_to event_contact_url(EventContact.last)
  end

  test "should show event_contact" do
    get event_contact_url(@event_contact)
    assert_response :success
  end

  test "should get edit" do
    get edit_event_contact_url(@event_contact)
    assert_response :success
  end

  test "should update event_contact" do
    patch event_contact_url(@event_contact), params: { event_contact: { event_id: @event_contact.event_id, person_id: @event_contact.person_id, role: @event_contact.role } }
    assert_redirected_to event_contact_url(@event_contact)
  end

  test "should destroy event_contact" do
    assert_difference('EventContact.count', -1) do
      delete event_contact_url(@event_contact)
    end

    assert_redirected_to event_contacts_url
  end
end
